from multiprocessing import Process
from erlport.erlterms import Atom
from flaskapp import app

p = Process(target=app.run)


def start():
    p.start()
    return Atom("ok")


def stop():
    global p
    p.terminate()
    p = Process(target=app.run)
    return Atom("ok")
