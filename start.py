import os
import subprocess

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


def rel(*x):
    return os.path.join(BASE_DIR, *x)


my_env = os.environ.copy()

my_env['PYTHON_BIN'] = os.path.abspath(rel('venv/bin/python'))
my_env['PYTHON_PATH'] = os.path.abspath(rel('.'))

subprocess.check_call(
    ['cowboyapp/_rel/cowboyapp_release/bin/cowboyapp_release', 'start'],
    env=my_env
)
