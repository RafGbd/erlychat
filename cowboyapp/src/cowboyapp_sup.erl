-module(cowboyapp_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).


init([]) ->
    Procs = [
        {users_online, {users_online, start, []}, permanent, 1000, worker, []},
        {flask_backend, {flask_backend, start, []}, permanent, infinity, worker, []}
    ],
    {ok, {{one_for_one, 1, 5}, Procs}}.