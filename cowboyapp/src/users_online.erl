-module(users_online).
-behaviour(gen_server).


-export([start/0, stop/0, add/2, del/1, get_pids/0, get_sessions/0]).
-export([init/1, handle_call/3, handle_cast/2,
         handle_info/2, terminate/2, code_change/3]).



start() ->
    gen_server:start_link({global, ?MODULE}, ?MODULE, [], []).

stop() ->
    gen_server:cast(?MODULE, shutdown).


add(SessionId, Pid) ->
    gen_server:call({global, ?MODULE}, { add, SessionId, Pid }).

del(SessionId) ->
    gen_server:call({global, ?MODULE}, { del, SessionId }).

get_sessions() ->
    gen_server:call({global, ?MODULE}, { get_sessions }).

get_pids() ->
    gen_server:call({global, ?MODULE}, { get_pids }).


init([]) ->
  State = dict:new(),
  {ok, State}.

handle_call({ add, SessionId, Pid }, _From, State) ->
    NewState = dict:store(SessionId, Pid, State),
    { reply, ok, NewState };

handle_call({ del, SessionId }, _From, State) ->
  NewState = dict:erase(SessionId, State),
  { reply, ok, NewState };


handle_call({ get_sessions }, _From, State) ->
    Resp = dict:fetch_keys(State),
    { reply, Resp, State };

handle_call({ get_pids }, _From, State) ->
    Items = dict:to_list(State),
    Resp = lists:map(fun({_S, Pid}) -> Pid end, Items),
    { reply, Resp, State };

handle_call(_Message, _From, State) ->
  { reply, invalid_command, State }.

handle_cast(_Message, State) -> { noreply, State }.
handle_info(_Message, State) -> { noreply, State }.
terminate(_Reason, _State) -> ok.
code_change(_OldVersion, State, _Extra) -> { ok, State }.