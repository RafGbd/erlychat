-module(broadcast_message_handler).
-behaviour(cowboy_http_handler).

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).

-record(state, {
}).

init(_, Req, _Opts) ->
    {ok, Req, #state{}}.

handle(Req, State=#state{}) ->
    Resp = case cowboy_req:has_body(Req) of
        true ->
            {ok, RequestBody, _} = cowboy_req:body(Req),
            Data = binary_to_list(RequestBody),
            Pids = users_online:get_pids(),
            [Pid ! Data || Pid <- Pids],
            jsone:encode([{result, ok}]);
        false ->
            jsone:encode([{result, error}])
    end,


    _Pids = users_online:get_pids(),
    
    {ok, Req2} = cowboy_req:reply(200,
        [{<<"content-type">>, <<"application/json">>}],
        Resp,
        Req),
    {ok, Req2, State}.

terminate(_Reason, _Req, _State) ->
    ok.