-module(cowboyapp_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).


start(_Type, _Args) ->
    Dispatch = cowboy_router:compile([
        {'_', [
            {"/users_online/", users_online_handler, []},
            {"/message_broadcast/", broadcast_message_handler, []},
            {"/ws/", websocket_handler, []},
            {'_', proxy_handler, []}
        ]}
    ]),
    cowboy:start_http(my_http_listener, 100, [{port, 9000}],
        [{env, [{dispatch, Dispatch}]}]
    ),
    ssl:start(),
    inets:start(),
    cowboyapp_sup:start_link().


stop(_State) ->
	ok.