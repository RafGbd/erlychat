-module(flask_backend).
-behaviour(gen_server).


-export([start/0, get_python/0]).
-export([init/1, handle_call/3, handle_cast/2,
         handle_info/2, terminate/2, code_change/3]).

% vvvv API vvvv

start() ->
    gen_server:start_link({global, ?MODULE}, ?MODULE, [], []).

get_python() ->
    gen_server:call({global, ?MODULE}, { get_python }).



init([]) ->
  process_flag(trap_exit, true),
  {ok, P} = python:start([
      {python_path, [os:getenv("PYTHON_PATH")]},
      {python, os:getenv("PYTHON_BIN")}

  ]),
  python:call(P, backend, start, []),
  {ok, P}.

terminate(_Reason, P) ->
  python:call(P, backend, stop, []),
  python:stop(P),
  ok.

handle_call({ get_python }, _From, P) ->
    { reply, P, P };

handle_call(_Message, _From, State) ->
  { noreply, State }.

handle_cast(_Message, State) -> { noreply, State }.
handle_info(_Message, State) -> { noreply, State }.
code_change(_OldVersion, State, _Extra) -> { ok, State }.