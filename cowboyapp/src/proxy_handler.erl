-module(proxy_handler).
-behaviour(cowboy_http_handler).

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).

-record(state, {
}).

init(_, Req, _Opts) ->
	{ok, Req, #state{}}.


cowboy_method_to_httpc(<<"GET">>) -> get;
cowboy_method_to_httpc(<<"POST">>) -> post;
cowboy_method_to_httpc(<<"HEAD">>) -> head;
cowboy_method_to_httpc(<<"OPTIONS">>) -> options;
cowboy_method_to_httpc(<<"PATCH">>) -> patch;
cowboy_method_to_httpc(<<"PUT">>) -> put;
cowboy_method_to_httpc(<<"DELETE">>) -> delete.

get_method(Req) ->
    {CowboyMethod, _} = cowboy_req:method(Req),
    cowboy_method_to_httpc(CowboyMethod).

patch_location_header({"location", Location}) ->
    {"location", re:replace(Location, "http://localhost:5000/", "http://localhost:9000/", [{return,list}])};
patch_location_header(Header) ->
    Header.

convert_request_header({<<"host">>,<<"localhost:9000">>}) ->
    {"host","localhost:5000"};
convert_request_header({Key, Value}) ->
    {binary_to_list(Key), binary_to_list(Value)}.


handle(Req, State=#state{}) ->
    Method = get_method(Req),
    BackendHost = "http://localhost:5000",
    {Path, _} = cowboy_req:path(Req),
    BackendUrl = BackendHost ++ binary_to_list(Path),
    {RequestHeaders, _} = cowboy_req:headers(Req),
    RequestHeaders1 = [convert_request_header(H) || H <- RequestHeaders],
    Request = case cowboy_req:has_body(Req) of
        true ->
            {ContentType, _} = cowboy_req:header(<<"content-type">>, Req, <<"text/plain">>),
            {ok, RequestBody, _} = cowboy_req:body(Req),
            {BackendUrl, RequestHeaders1, binary_to_list(ContentType), binary_to_list(RequestBody)};
        false ->
            {BackendUrl, RequestHeaders1}
    end,

    Resp = httpc:request(Method, Request, [{autoredirect, false}], []),

    {Result, {{_HttpVersion, StatusCode, _ReasonPhrase}, Headers, Body}} = Resp,

    ResponseHeaders = case StatusCode of 
        302 -> 
            [patch_location_header(H) || H <- Headers];
        _ ->
            Headers
    end,

    {Result, Req2} = cowboy_req:reply(StatusCode,
        ResponseHeaders,
        Body,
        Req),
    {Result, Req2, State}.


terminate(_Reason, _Req, _State) ->
	ok.
