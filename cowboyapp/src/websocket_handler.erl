-module(websocket_handler).
-behaviour(cowboy_websocket_handler).


-export([init/3]).
-export([websocket_init/3]).
-export([websocket_handle/3]).
-export([websocket_info/3]).
-export([websocket_terminate/3]).

-record(state, {
}).

init(_, _Req, _Opts) ->
    {upgrade, protocol, cowboy_websocket}.

websocket_init(_Type, Req, _Opts) ->
    {SessionId, _} = cowboy_req:cookie(<<"session_id">>, Req),
    users_online:add(SessionId, self()),
    {ok, Req, #state{}}.

websocket_handle(_InFrame, Req, State) -> {ok, Req, State}.

websocket_info(Message, Req, State) -> 
    {reply, {text, Message}, Req, State}.

websocket_terminate(_Reason, Req, _State) ->
    {SessionId, _} = cowboy_req:cookie(<<"session_id">>, Req),
    users_online:del(SessionId),
    ok.

