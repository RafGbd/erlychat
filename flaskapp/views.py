import hashlib
import requests
from datetime import datetime
from flask import render_template
from flask import request, redirect, abort
from flaskapp import app
from flaskapp import db
from flaskapp.models import User, Message


@app.route('/', methods=['GET', 'POST'])
def index():

    if request.method == 'POST':
        username = request.form.get('username')
        if username:
            # dummy session
            session_id = hashlib.md5(username).hexdigest()

            user = User.query.filter(User.username == username).first()
            if user:
                user.session_id = session_id
            else:
                user = User(username=username, session_id=session_id)

            db.session.add(user)
            db.session.commit()

            redirect_to_chat = redirect('/chat/')
            response = app.make_response(redirect_to_chat)
            response.set_cookie('session_id', value=session_id)
            return response

    return render_template("signin.html")


@app.route('/chat/')
def chat():
    session_id = request.cookies.get('session_id')
    user = User.query.filter(User.session_id == session_id).first()
    if not user:
        return abort(403)

    resp = requests.get("http://127.0.0.1:9000/users_online/")

    sessions_online = resp.json()

    users_online = User.query.filter(
        User.session_id.in_(sessions_online)
    ).all()

    messages = Message.query.all()

    return render_template(
        "chat.html",
        messages=messages,
        users_online=users_online
    )


@app.route('/chat/submit_message/', methods=['POST'])
def submit_message():
    session_id = request.cookies.get('session_id')
    user = User.query.filter(User.session_id == session_id).first()
    if not user:
        return abort(403)

    message = request.form.get('message')
    if message:
        msg = Message(
            body=message,
            user_id=user.id,
            created_at=datetime.utcnow()
        )
        resp = requests.post(
            'http://127.0.0.1:9000/message_broadcast/',
            json={
                "body": msg.body,
                "user": user.username,
                "created_at": str(msg.created_at),
            })
        if resp.ok and resp.json()['result'] == 'ok':
            db.session.add(msg)
            db.session.commit()
            return "ok"
    return abort(400)
