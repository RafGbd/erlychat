from datetime import datetime
from flaskapp import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    session_id = db.Column(db.String(32), unique=True, nullable=False)

    def __repr__(self):
        return '<User %r>' % self.username


class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text, nullable=False)
    created_at = db.Column(
        db.DateTime, nullable=False,
        default=datetime.utcnow
    )

    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id'),
        nullable=False
    )
    user = db.relationship(
        'User',
        backref=db.backref('messages', lazy=True)
    )

    def __repr__(self):
        return '<Message %r>' % self.body
