Системные зависимости: erlang, rebar, python, virtualenv, make

Сборка проекта: make setup

Запуск проекта: make start

Фронтенд расположен по адресу: http://localhost:9000

Остановка проекта: make stop

