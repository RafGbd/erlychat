setup:
	virtualenv ./venv
	venv/bin/pip install -r ./flaskapp/requirements.txt
	venv/bin/python init_db.py
	$(MAKE) -C ./cowboyapp/

start:
	python start.py

stop:
	bash -c "./cowboyapp/_rel/cowboyapp_release/bin/cowboyapp_release stop"